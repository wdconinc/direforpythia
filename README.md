# Welcome to Dire

## Introduction

Dire is a C++ program for simulating radiation cascades for particle physics. 
This page hosts the core code of the Dire plugin to the Pythia event
generator. 

## Important warning
This repository is for development and testing. For stable and validated versions, see the Releases section below. 

## Releases

For documentation and releases, please consult [dire.gitlab.io](https://dire.gitlab.io)

You can also obtain (at your own risk) specific releases directly from this repository, by e.g. executing

git clone --depth 1 --no-checkout git@gitlab.com:dire/direforpythia.git && cd direforpythia && git --work-tree=. checkout master -- tags/2.004 && cd ../; mv direforpythia/tags/2.004 DIRE-2.004 && rm -rf direforpythia

in your terminal.

## Authors

Dire has been developed by Stefan Hoeche and Stefan Prestel. The latter will take all blame
regarding the Dire plugin for the Pythia 8 event generator, i.e. for the code in this repository.

## News:
- 2019/10/14: Dire version 2.004 is now released. Enjoy this new, improved version!
              This version is compatible with Pythia 8.243 and consolidates the features included in version 2.003. In particular, iterated matrix-element corrections, QED showering and NLO merging are now encouraged. For usage suggestions, please consult [dire.gitlab.io](https://dire.gitlab.io). Thanks to Mikhail Kirsanov for reporting some inconsistencies between Pythia 8.243 and Dire 2.003, which prompted this release.
- 2019/01/04: Dire version 2.003 is now released. Enjoy this new, improved version!
              This version is compatible with Pythia 8.240 and includes many new features, especially extended iterated matrix-element correction capabibities, and the introduction of QED showering. Please note that matrix-element corrections are only applied for configurations accessible with an ordered parton shower, and thus do not lead to any formal improvement of the accuracy or precision of the code. More details can be found in the UpdateNotes.txt file.
- 2018/06/04: Dire version 2.002 is now released. Enjoy this new, improved version!
              This version includes important fixes for showers off resonance decay products for which the recoiler does not share the same color charges. In this case, the splitting kernels should contain all collinear and soft divergences. This addressed an issue pointed out by Markus Seidel and observed in CMS-PAS-TOP-17-013. Thanks to Markus Seidel and CMS for highlighting this. The version further includes various bug fixes. Note that any code changes related to double-soft QCD corrections and QED are under development. Both effects are switched off by default, and should be regarded as incorrect in both implementation and results. The author does at this point not give any guarantees regarding the implementation of double-soft QCD or QED.
- 2018/02/02: Fix of NON-SENSITIVE handling of shared libraries for Dire version 2.001. This allows to correctly link against and build shared libraries. Thanks to Giancarlo Panizzo and Markus Seidel for suggesting!
- 2018/01/15: Fix of sensitive bug for Dire version 2.001! This removes rare cases of segmentation faults in timelike showers. Thanks to Giancarlo Panizzo for reporting!
- 2017/09/10: Dire version 2.001 is now released! Enjoy this new, improved version!
- 2017/08/18: Created a gitlab repository for DireForPythia

