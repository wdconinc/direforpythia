
#include "Dire/Splittings.h"
#include "Dire/DireSpace.h"
#include "Dire/DireTimes.h"

namespace Pythia8 {

//==========================================================================

// The Splitting class.

//--------------------------------------------------------------------------

void DireSplitting::init() {

  renormMultFac      = 1.;
  if (id.find("Dire_isr_") != string::npos)
    renormMultFac    = settingsPtr->parm("SpaceShower:renormMultFac");
  else
    renormMultFac    = settingsPtr->parm("TimeShower:renormMultFac");

  if ( id.find("_qcd_")      != string::npos) is_qcd  = true;
  if ( id.find("_qed_")      != string::npos) is_qed  = true;
  if ( id.find("_ew_")       != string::npos) is_ewk  = true;
  if ( id.find("Dire_")      != string::npos) is_dire = true;
  if ( id.find("Dire_isr_")  != string::npos) is_isr  = true;
  if ( id.find("Dire_fsr_")  != string::npos) is_fsr  = true;

  nameHash = shash(id);

}

//--------------------------------------------------------------------------

double DireSplitting::getKernel(string key) {
  unordered_map<string, double>::iterator it = kernelVals.find(key);
  if ( it == kernelVals.end() ) return 0./0.;
  return it->second;
}

//--------------------------------------------------------------------------

void DireSplitting::storeKernelVals(
  unordered_map<string,double> in) {
  // Reset all kernel values ifany entry is not well-defined.
  bool isgood = true;
  for ( unordered_map<string,double>::iterator it = in.begin();
    it != in.end(); ++it ) if (std::isnan(it->second)) isgood = false; 
  if (!isgood) { 
   string message = "Warning in Dire splitting kernel ";
   message += name();
   message += ": Not-a-number splitting probability. Force to zero.";
   infoPtr->errorMsg(message);
   for ( unordered_map<string,double>::iterator it = in.begin();
     it != in.end(); ++it ) it->second = 0.0; 
  }
  // Store kernel values.
  clearKernelVals();
  for ( unordered_map<string,double>::iterator it = in.begin();
    it != in.end(); ++it )
    kernelVals.insert(make_pair( it->first, it->second ));
}

//--------------------------------------------------------------------------

void DireSplitting::storeKernelVal(string key, double val) {
  // Reset all kernel values ifany entry is not well-defined.
  bool isgood = true;
  if (std::isnan(val)) isgood = false; 
  unordered_map<string, double>::iterator it = kernelVals.find(key);
  if (!isgood) { 
   string message = "Warning in Dire splitting kernel ";
   message += name();
   message += ": Not-a-number splitting probability. Force to zero.";
   infoPtr->errorMsg(message);
   val = 0.0;
  }
  if ( it == kernelVals.end() ) kernelVals.insert(make_pair(key,val));
  else it->second = val;
}

//==========================================================================

} // end namespace Pythia8
