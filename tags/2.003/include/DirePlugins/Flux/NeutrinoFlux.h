// DireNeutrinoFlux.h is a part of the PYTHIA event generator.
// Copyright (C) 2018 Stefan Prestel, Torbjorn Sjostrand.
// PYTHIA is licenced under the GNU GPL version 2, see COPYING for details.
// Please respect the MCnet Guidelines, see GUIDELINES for details.

// Author: Stefan Prestel, Pedro Machado.

#ifndef Pythia8_DireNeutrinoFlux_H
#define Pythia8_DireNeutrinoFlux_H

// Includes
#include "Pythia8/Pythia.h"

namespace Pythia8 {

//==========================================================================

// A simple scaling PDF. Not realistic; only to check that it works.

class MiniBooneNeutrinoFlux : public PDF {

public:

  // Constructor.
  MiniBooneNeutrinoFlux(int idBeamIn = 2212) : PDF(idBeamIn) {}

private:

  // Update PDF values.
  void xfUpdate(int id, double x, double Q2);

};

//--------------------------------------------------------------------------

// No dependence on Q2, so leave out name for last argument.

void MiniBooneNeutrinoFlux::xfUpdate(int id, double x, double ) {

  xlepton = 0.;
  // This is x*f(x)
  if (abs(id) == 12) {
    if(0.6 - 5*x<0) xlepton = x*(35.365207899973*pow(exp(-108.13148788927333*pow(-0.12 + x,2)),2.5)-2.005e-17);
    if(0.6 - 5*x>=0)xlepton = x*(35.36520789997301*exp(-54.25347222222223*pow(-0.12 + x,2))-2.005e-17);
    xlepton = max(xlepton,1e-15);
  }

  // No quarks, gluons, etc in flux
  xg = xu = xd = xubar = xdbar = xs = xc = xb = xuVal = xuSea = xdVal
     = xdSea = 0.;

  // idSav = 9 to indicate that all flavours reset.
  idSav = 9;

}

//==========================================================================

} // end namespace Pythia8

#endif
